import 'package:flutter/material.dart';
import 'package:flutter_common/utils.dart';

class FullscreenButton extends StatelessWidget {
  final bool _opened;
  final Color? color;
  final VoidCallback? onTap;

  const FullscreenButton.close({this.color, this.onTap}) : _opened = true;

  const FullscreenButton.open({this.color, this.onTap}) : _opened = false;

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(_opened ? Icons.fullscreen_exit : Icons.fullscreen),
        color: color,
        onPressed: () {
          _opened ? onlyPortrait() : onlyLandscape();
          onTap?.call();
        });
  }
}
