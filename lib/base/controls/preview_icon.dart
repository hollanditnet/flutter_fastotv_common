import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

enum PreviewType { LIVE, VOD }

class PreviewIcon extends StatelessWidget {
  final String link;
  final double? height;
  final double? width;
  final PreviewType type;

  const PreviewIcon.live(this.link, {this.height, this.width}) : type = PreviewType.LIVE;

  const PreviewIcon.vod(this.link, {this.height, this.width}) : type = PreviewType.VOD;

  String assetsLink() {
    if (type == PreviewType.LIVE) {
      return 'install/assets/unknown_channel.png';
    } else if (type == PreviewType.VOD) {
      return 'install/assets/unknown_preview.png';
    } else {
      return 'install/assets/unknown_channel.png';
    }
  }

  Widget defaultIcon() {
    return Image.asset(assetsLink(), height: height, width: width);
  }

  @override
  Widget build(BuildContext context) {
    final image = CachedNetworkImage(
        imageUrl: link,
        placeholder: (context, url) => defaultIcon(),
        errorWidget: (context, url, error) => defaultIcon(),
        height: height,
        width: width);
    return ClipRect(child: image);
  }
}
